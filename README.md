# Blog
Using CakePHP v1.3 to build a blogging web application.

![home](home.png)

## [Requirements](https://gist.github.com/ericfledderman/6c4f0f7e6ffa3477372ebf5392bad6cd)
- PHP v5.6
- MySQL v5.7
- [composer](https://getcomposer.org)

## From Ground Up
1. Setup database.
2. Install dependencies libraries through composer.
3. Create symlinks to cake.
4. Bake application at `/projects/cakeblog/app`.
5. Create symlinks to simpletest.
6. Done.

## Getting Started
1. Checkout source code.
2. Setup database.
3. Install dependencies libraries through composer.
4. Done.

```bash
$ git clone https://gitlab.com/hornetportal/cakeblog.git``
$ cd cakeblog/bin
$ mysql -u root -p
mysql > create database blog;
mysql > use blog;
mysql > source database.sql;
mysql > exit;
$ cd ..
$ curl -sS https://getcomposer.org/installer | php
$ php composer.phar install
$ ln -s vendors/cakephp/cake cake
$ cake/console/cake bake
$ cd app/vendors
$ ln -s ../../vendors/simpletest simpletest
$ cd ..
$ ../cake/console/cake bake all
```

## Run Test at Console
```bash
$ cd app
$ ../cake/console/cake testsuite app all
$ ../cake/console/cake testsuite app case controllers/posts_controller
$ ../cake/console/cake testsuite app case models/post
```

## Install Gitlab Runner on Ubuntu
1. Install gitlab runner.
2. Create gitlab-runner as user.
3. Register runner with
 - url - https://gitlab.com
 - specific `ubuntu` as tag
 - Find the token at https://gitlab.com/hornetportal/cakeblog/-/settings/ci_cd
 - Choose shell
4. Start runner.
5. Done.

```bash
$ sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
$ sudo chmod +x /usr/local/bin/gitlab-runner
$ sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
$ sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
$ sudo gitlab-runner register
$ sudo gitlab-runner start
```

## References
- https://book.cakephp.org/1.3/en/The-Manual/Developing-with-CakePHP.html
- [SimpleTest in CakePHP](https://book.cakephp.org/1.3/en/The-Manual/Common-Tasks-With-CakePHP/Testing.html)
- [Instal Gitlab Runner](https://docs.gitlab.com/runner/install/linux-manually.html)