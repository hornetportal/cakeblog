<?php
/* Posts Test cases generated on: 2020-06-16 14:52:48 : 1592290368*/
App::import('Controller', 'Posts');

class TestPostsController extends PostsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class PostsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.post');

	function startTest() {
		$this->Posts =& new TestPostsController();
		$this->Posts->constructClasses();
	}

	function endTest() {
		unset($this->Posts);
		ClassRegistry::flush();
	}

	function testIndex() {
		$result = $this->testAction('/Posts/index', array(
			'fixturize' => false,
			'method' => 'get',
			'return' => 'vars'
		));
		debug($result);
		$this->assertTrue(count($result['posts']) > 0);
	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
